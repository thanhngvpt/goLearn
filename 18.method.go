/**
 * - notes:
 	- methods with pointer receiver can take either a value or a pointer if take a value it will automatically call to (&value) (pointer)
 		but function with pointer param cant.
 	- method with value receiver can take either a value or a pointer and its automatically interpretered to (*value) (value)
 */

package main

import (
	"math"
	"fmt"
)

type Vertex struct {
	X, Y float64
}

// value receiver method: this work on copy of value of receiver
func (v Vertex) Abs() float64  {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

// pointer receiver method: working direct on value of receiver
func (v *Vertex) Scale(f float64)  {
	v.X *= f
	v.Y *= f
}

func Abs(v Vertex) float64  {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}
func Scale(v *Vertex, f float64) {
	v.X *= f
	v.Y *= f
}

func main() {
	v := Vertex{3, 4}

	// use value v for pointer receiver in method
	v.Scale(4) // this will automatically call (&v).Scale(4)
	fmt.Println("changed v: ", v)
	p := &v
	fmt.Println(p.Abs()) // work well because this automatically interpreted to (*p).Abs()

}