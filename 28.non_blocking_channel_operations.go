/**
 * Basic sends and receives on channels are blocking. we can use default on select to non blocking
 */

package main

import "fmt"

func main() {
	message := make(chan string)
	signal := make(chan bool)

	// non blocking receive
	select {
	case msg := <- message:
		fmt.Println("received message: ", msg)
	default:
		fmt.Println("No message received")
	}

	// non blocking send
	msg := "Hi"
	select {
	case message <- msg:
		fmt.Println("Sent message: ", msg)
	default:
		fmt.Println("No message sent")
	}

	// use multiple case to implement a multi-way non blocking
	select {
	case msg := <- message:
		fmt.Println("Received message: ", msg)
	case signal := <- signal:
		fmt.Println("Received signal: ", signal)
	default:
		fmt.Println("No activity")
	}
}
