package main

import "fmt"

// closure: the intSeq return another function, which we define anonymously in the body of intSeq
// the returned function close over the variable i to form a closure
func intSeq() func() int {
	i := 0

	return func() int {
		i += 1
		return i
	}
}

func main() {
	// call closure
	nextInt := intSeq()
	fmt.Println(nextInt)
	fmt.Println(nextInt())
	fmt.Println(nextInt())
	fmt.Println(nextInt())

	// closure is unique to particular function
	otherInt := intSeq()
	fmt.Println(otherInt)
	fmt.Println(otherInt())
	fmt.Println(otherInt())
	fmt.Println(otherInt())
}
