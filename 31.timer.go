/**
 * timer similar setTimeout and ticker similar setInterval in javascript
 */


package main

import (
	"time"
	"fmt"
)

func main() {
	// create a timer wait in 2s
	t1 := time.NewTimer(time.Second * 2)

	// block on timer's channel C until it sends a value indicateing that timer expired
	<- t1.C
	fmt.Println("Timer 1 expired")

	t2 := time.NewTimer(time.Second)
	go func() {
		<- t2.C
		fmt.Println("Timer 2 expired")
	}()

	// time.Sleep action similar timer but we can cancel it before it expired
	stop2 := t2.Stop()
	if stop2 {
		fmt.Println("Timer 2 stopped")
	}
}
