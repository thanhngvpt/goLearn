/**
 * Pointer notes:
 *		- *Type: create a pointer has type is Type
 		- &varName: get memory address of varName
 		- *ptrName: get/set value is storing in the memory address that ptrName pointer is pointing to
 */

package main

import "fmt"

// by value
func zeroVal(iVal int) {
	iVal = 0
}

// by address - reference
func zeroPointer(iPtr *int) {
	*iPtr = 0
}

func main() {
	a := 5
	var p *int = &a

	fmt.Printf("value of a: %d, address of a: %p, the value is storing in memory address of a: %d\n", a, &a, *(&a))
	fmt.Printf("value of p: %p, memory address of p: %p, the value is storing in memory address that p is pointing to: %d\n", p, &p, *p)
}
