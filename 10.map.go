package main

import "fmt"

func main() {
	// 1. create an empty map
	emptyMap := make(map[string]int)

	// 2. set/get map
	m := make(map[string]int)
	m["k1"] = 1
	m["k2"] = 2

	// 3. delete key
	d := make(map[string]int)
	d["k1"] = 1
	d["k2"] = 2

	delete(d, "k1")

	// 4. get value from map
	v, ok := d["k1"]

	// 5. declare and init
	mm := map[string]int{
		"k1": 1,
		"k2": 2,
	}

	fmt.Println(emptyMap, m, d, v, ok, mm)
}
