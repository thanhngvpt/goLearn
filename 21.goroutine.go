package main

import (
	"fmt"
	//"time"
)

func f(s string) {
	for i := 0; i < 3; i++ {
		fmt.Println(s, ":", i)
	}
}

func main() {

	// synchronously function call
	f("Direct")

	// concurrently function call - start a new goroutine
	go f("Goroutine")

	// start a go routine for an anonymous function call
	go func(msg string) {
		fmt.Println(msg)
	}("Going")

	// below statement to wait goroutiens finish to display result
	var input string
	fmt.Scanln(&input)
	fmt.Println("Done")
	// or
	//time.Sleep(5000 * time.Millisecond)
}
