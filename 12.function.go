package main

import "fmt"

// 1. normal function with params and return value
func plus(a int, b int) int {
	return a + b
}

// 2. omit type of params in front of last param has the same type
func multiplePlus(a, b, c int) int {
	return a + b + c
}

// 3. multiple return values
func add(a, b int) (int, error) {
	return a +b, nil
}

// 4. named return values
func sumary(a, b int) (multi int, minus int) {
	multi = a + b
	minus = a - b
	return
}

func main() {
	// call function
	fmt.Println(plus(2, 3), multiplePlus(2, 3, 4))

	sum, err := add(2, 3)
	fmt.Println(sum, err)

	fmt.Println(sumary(3, 4))
}
