package main

import "fmt"

// 1. varidic functions
func totalSum(nums...int) int {
	total := 0
	for _, v := range nums {
		total += v
	}
	return total
}

func main() {
	fmt.Println(totalSum(1, 2))
	fmt.Println(totalSum(1, 2, 3, 4))
	args := []int{1, 3, 5, 7, 9}
	fmt.Println(
		totalSum(
			args..., // this will extract elements into a slice
		),
	)
}