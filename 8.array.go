package main

import "fmt"

func main() {
	// array in golang has fixed length, its mean the size of an array cant be change

	// 1. declare an array wih zero-value
	var zeroValuesArr [5]int

	// 2. declare and init value for array
	valuesArr := [5]int{1, 2, 3, 4, 5}

	// 3. multiple dimension array
	matrix := [2][3]int{
		{1, 2, 3},
		{4, 5, 6},
	}

	fmt.Println(zeroValuesArr, valuesArr, matrix)
}
