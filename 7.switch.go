package main

import (
	"fmt"
	"time"
)

func main() {
	// 1. basic switch
	i := 2
	switch i {
	case 1:
		fmt.Println("one")
	case 2:
		fmt.Println("two")
	case 3:
		fmt.Println("three")
	default:
		fmt.Println("Unkown")
	}

	// 2. use comma in case
	switch time.Now().Weekday() {
	case time.Saturday, time.Sunday:
		fmt.Println("Weekend")
	default:
		fmt.Println("A week day")
	}

	// 3. switch withou expression is a if..else
	now := time.Now().Hour()
	switch  {
	case now > 19:
		fmt.Println("Evening")
	case now > 12:
		fmt.Println("Afternoon")
	case now > 6:
		fmt.Println("Morning")
	default:
		fmt.Println("Night")
	}
}
