package main

import "fmt"

func main() {
	// 1. create a slice with none-zero length
	noneZeroLength := make([]string, 3)

	// 2. append one or more elements to slice
	a := make([]string, 3)
	a = append(a, "a")
	a = append(a, "b", "c")

	b := make([]int, 3)
	b = append(b, 4)

	// 3. copy slice
	c := make([]string, len(a))
	copy(c, a)

	// 4. slice operator
	d := c[2:]

	// 5. declare and init slice
	e := []string{"x", "y", "z", "w"}

	// 6. multiple dimensional slice: note the length of inner slice can vary unlike array
	m := make([][]int, 3)
	for i:= 0; i < 3; i++ {
		innerLen := i + 1
		m[i] = make([]int, innerLen)
		for j := 0; j < innerLen; j++ {
			m[i][j] = i+ j
		}
	}

	fmt.Println(noneZeroLength, a, b, c, d, e, m)
}
