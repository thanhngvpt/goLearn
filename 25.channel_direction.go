package main

import (
	"fmt"
	"time"
)

// this function only accept a channel for sending value
func ping(pings chan <- string, msg string) {
	pings <- msg
	time.Sleep(time.Second * 10)
}

// this function only accept a channel for receive value named pings and pongs for only send
func pong(pings <- chan string, pongs chan <- string) {
	msg := <- pings
	pongs <- msg + " from pongs"
}

func main() {
	pings := make(chan string, 1)
	pongs := make(chan string, 1)

	ping(pings, "A passed message")
	pong(pings, pongs)

	fmt.Println(<-pongs)
}
