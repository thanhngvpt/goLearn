package main

import (
	"fmt"
	"errors"
)

func f(a int) (int, error) {
	if a < 0 {
		return -1, errors.New("aaa")
	}

	return a + 1, nil
}

func main() {
	fmt.Println(f(-3))
	fmt.Println(f(88))
}