/**
 * struct are named collections of fields
 */

package main

import "fmt"

// define a struct
type Person struct {
	name string
	age int
}

func main() {
	// create new struct then print
	fmt.Println(Person{"Bob", 20})

	// create new struct by name the fields
	fmt.Println(Person{name: "David", age: 27})

	// omitted fields will be zero-valued
	fmt.Println(Person{name: "John"})

	// & will yield a pointer to the struct
	fmt.Println(&Person{name: "Yeild", age: 40})

	// access struct fields with a dot
	s := Person{name: "Smith", age: 53}
	fmt.Println(s.name)

	// access struct fields via its pointer with dot
	sp := &s
	fmt.Println(sp.age)

	// structs are mutable
	sp.age = 55
	s.name = "Peter"
	fmt.Println(sp)


}
