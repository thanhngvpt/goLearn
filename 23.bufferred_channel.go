/**
 * In default channel is unbuffered, meaning that they will only accept sends if there is a corresponding receive ready to receive
 * the sent value. buffered channel accept a limited number of values without a corresponding receiver
 */

package main

import "fmt"

func main() {
	// create a new buffered channel with limited is 2 value
	message := make(chan string, 2)

	// send values to channel
	message <- "First"
	message <- "Second"

	// receive values from channel twice
	fmt.Println(<- message) // first
	fmt.Println(<- message)
}
