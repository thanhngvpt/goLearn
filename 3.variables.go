package main

import "fmt"

func main() {
	// declare a variable and init it.
	var s string = "initial"

	// declate multiple variables
	var a, b int  = 1, 2

	// declare variable without data type. go compiler will infer type for that variable by type of values it assigned
	var d = true

	// variables will get zero-values base on data type if we are not init its value
	var i int

	// shorthand for declare and init a variable
	shortHand := "short"

	fmt.Println(s, a, b, d, i, shortHand)
}
