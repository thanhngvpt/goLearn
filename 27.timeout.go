/**
 * timeout in golang be implemented easy and elegant by using channels and select
 */

package main

import (
	"time"
	"fmt"
)

func main() {
	c1 := make(chan string, 1)

	// suppose this goroutine will call to external resource and done after 2s then send result to channel c1
	go func() {
		time.Sleep(time.Second * 2)
		c1 <- "result 1"
	}()

	// implement timeout
	select {
	case result := <- c1:
		fmt.Println("c1 ", result)
	case <- time.After(time.Second * 1):
		fmt.Println("timeout 1s")
	}

	//////////////
	c2 := make(chan string, 1)
	go func() {
		time.Sleep(time.Second * 2)
		c2 <- "result 2"
	}()

	select {
	case result := <- c2:
		fmt.Println("c2 ", result)
	case <- time.After(time.Second * 3):
		fmt.Println("timeout 3s")
	}
}
