package main

import (
	"fmt"
	"math"
)

const s string = "constant"

func main() {
	const n = 5000
	const d = 3e20/n

	fmt.Println(math.Sin(d))
}
