import React from 'react';
import ReactDOM from 'react-dom';
import {Router, Route, Link, browserHistory, IndexRoute} from 'react-router';

export class App extends React.Component {
    render() {
        return (
            <div>
                <ul>
                    <li><a href="/">Homepage</a></li>
                    <li><a href="/home">Home</a></li>
                    <li><a href="/about">About</a></li>
                    <li><a href="/contact">Contact</a></li>
                </ul>
                {this.props.children}
            </div>
        );
    };
}

export class Home extends React.Component {
    render() {
        return (
            <div>
                <h1>Home ...</h1>
            </div>
        );
    };
}

export class About extends React.Component {
    render() {
        return (
            <div>
                <h1>About ...</h1>
            </div>
        );
    };
}

export class Contact extends React.Component {
    render() {
        return (
            <div>
                <h1>Contact ...</h1>
            </div>
        );
    };
}
