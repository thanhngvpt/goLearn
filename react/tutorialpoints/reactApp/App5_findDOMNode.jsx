import React from 'react';
import ReactDOM from 'react-dom';

class App extends React.Component {
	constructor() {
		super();
		this.findDomNodeHandler = this.findDomNodeHandler.bind(this);
	}

	findDomNodeHandler() {
		var myDiv = document.getElementById('myDiv');
		ReactDOM.findDOMNode(myDiv).style.color = 'red';
	}

	render() {
		return(
			<div>
				<button onClick = {this.findDomNodeHandler}>Find Dome Node</button>
				<div id="myDiv">Node</div>
			</div>
		);
	}
}

export default App;