import React from 'react';

class App extends React.Component {
    constructor() {
        super();

        this.state = {
            data: [
                {id: 1, component: "One"},
                {id: 2, component: "Two"},
                {id: 3, component: "Three"},
            ]
        };
    };

    render() {
        return (
            <div>
                <div>
                    {this.state.data.map((obj, index) => 
                        <Content key = {index} componentData = {obj}></Content>)}
                </div>
            </div>
        );
    };
}

class Content extends React.Component {
    render() {
        return (
            <div>
                {this.props.componentData.id} - {this.props.componentData.component}
            </div>
        );
    };
}

export default App;