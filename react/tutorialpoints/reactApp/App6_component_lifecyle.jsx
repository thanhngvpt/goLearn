import React from 'react';

class App extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			data: 0
		};

		this.setNewNumber = this.setNewNumber.bind(this);
	}

	setNewNumber() {
		this.setState({data: this.state.data + 1});
		// console.log('a');
	}

	render() {
		return(
			<div>
				<button onClick = {this.setNewNumber}>Increment</button>
				<Content myNumber = {this.state.data}></Content>
			</div>
		);
	}

}

class Content extends React.Component {
	componentWillMount() {
	    console.log('compoennt will mount');  
	}

	componentDidMount() {
	 	console.log('component did mout');     
	}

	componentWillReceiveProps(nextProps) {
	    console.log('component will receive props');  
	}

	shouldComponentUpdate(nextProps, nextState) {
	    return true;  
	}

	componentWillUpdate(nextProps, nextState) {
	    console.log('component will update');
	}

	componentDidUpdate(prevProps, prevState) {
	    console.log('component did update');  
	}

	componentWillUnmount() {
	    console.log('component will unmount');  
	}

	render() {
		return (
			<div>
				<strong>{this.props.myNumber}</strong>
			</div>
		);
	}
}

export default App;