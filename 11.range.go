/**
 * range iterates over elements of variety data structure
 */

package main

import "fmt"

func main() {
	// 1. iteration on slice and array
	nums := []int{1, 2, 3}
	sum := 0
	for _, num := range nums {
		sum += num
	}
	fmt.Println("sum: ", sum)

	// 2. range on map
	m := map[string]string {
		"a": "Apple",
		"b": "Banana",
	}
	for key, value := range m {
		fmt.Printf("%s->%s\n", key, value)
	}

	// 3. range on string
	for index, value := range "Golang" {
		fmt.Printf("%d->%d\n", index, value)
	}
}
