/**
 * close a channel to indicate that no more values will be sent on it. this may useful to communicate completion tto channel's receivers
 */

package main

import "fmt"

func main() {
	jobs := make(chan int, 5)
	done := make(chan bool)

	go func() {
		for {
			j, more := <- jobs
			if more {
				fmt.Println("Received job: ", j)
			} else {
				fmt.Println("REceived all jobs")
				done <- true
				return
			}
		}
	}()

	for j := 1; j < 3; j++ {
		jobs <- j
		fmt.Println("sent job: ", j)
	}

	close(jobs)
	fmt.Println("sent all jobs")

	<- done
}
