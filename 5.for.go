package main

import "fmt"

func main() {
	// golang has only one loop construct is for it will responsible for while, do while ... as in other language

	// 1. single condition this as while in c
	i := 0
	for i < 3 {
		fmt.Println(i)
		i++
	}

	// 2. standard for with init, conds, after
	for j := 7; j <= 9; j++ {
		fmt.Println(j)
	}

	// 3. this as for (;;) {} in c
	for {
		fmt.Println("loop")
		break
	}
}
