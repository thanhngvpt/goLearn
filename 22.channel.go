package main

import "fmt"

func main() {
	// create a new channel with make
	message := make(chan string)

	// send value to message channel from a go routine
	go func() {
		message <- "Ping"
	}()

	// receive value from a channel
	msg := <- message

	fmt.Println(msg)

	/**
	 * Notes: In default sends and receives block util both sender and receiver are ready. This property allow us to wait at the end
	 * of our program
	 */
}
