package main

import (
	"fmt"
	"time"
)

func worker(done chan bool) {
	fmt.Println("Working...")
	time.Sleep(time.Second)
	fmt.Println("Done")

	// send finished notify to channel
	done <- true
}

func main() {
	// create buffered channel with limited is 1: this will make synchronization between a goroutine with others
	ch := make(chan bool, 1)

	// start a new goroutine
	go worker(ch)

	// block util goroutine above done
	if <-ch {
		fmt.Println("finished")
	}
}
